package com.techu.apirest.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class ProductoModelTest {

    @Test
    void testConstructor() {
        ProductoModel productoModel = new ProductoModel(1, 10.5, "Adidas", null);

        Assertions.assertEquals(1, productoModel.getId());
        Assertions.assertEquals(10.5, productoModel.getPrecio());
        Assertions.assertEquals("Adidas", productoModel.getDescripcion());
        Assertions.assertNull(productoModel.getUsuarios());
    }

    @Test
    void testSetters() {
        ProductoModel productoModel = new ProductoModel();
        productoModel.setId(1);
        productoModel.setPrecio(10.5);
        productoModel.setDescripcion("Adidas");
        productoModel.setUsuarios(new ArrayList<>());

        Assertions.assertEquals(1, productoModel.getId());
        Assertions.assertEquals(10.5, productoModel.getPrecio());
        Assertions.assertEquals("Adidas", productoModel.getDescripcion());
        Assertions.assertNotNull(productoModel.getUsuarios());
    }
}

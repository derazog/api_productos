package com.techu.apirest.service;

import com.techu.apirest.model.ProductoModel;
import com.techu.apirest.model.UsuarioModel;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class ProductoService {

    //Lista que simula persistencia de productos
    private static final List<ProductoModel> productoList = new ArrayList<>();
    //Utilitario que permite generar identificadores incrementales
    private final AtomicInteger secuenciaIds = new AtomicInteger(0);
    private final AtomicInteger secuenciaIdsUsuarios = new AtomicInteger(0);

    public List<ProductoModel> getProductos() {
        //Se retorna la lista no modificable para garantizar la integridad
        return Collections.unmodifiableList(productoList);
    }

    public void addProducto(ProductoModel producto) {
        producto.setId(this.secuenciaIds.incrementAndGet());

        //En caso se envíen usuarios, se les genera Ids
        if (producto.getUsuarios() != null && producto.getUsuarios().size() > 0) {
            producto.getUsuarios().forEach(usuarioModel -> usuarioModel.setId(secuenciaIdsUsuarios.incrementAndGet()));
        }

        productoList.add(producto);
    }

    public ProductoModel getProducto(int id) {
        for (ProductoModel producto : productoList) {
            if (producto.getId() == id) {
                return producto;
            }
        }
        //Si no se encuentra el producto, retorna null
        return null;
    }

    public void updateProducto(int id, ProductoModel productToUpdate) {
        for (int i = 0; i < productoList.size(); i++) {
            ProductoModel producto = productoList.get(i);
            if (producto.getId() == id) {
                //actualizamos el id del producto que reemplazará al antiguo
                productToUpdate.setId(id);
                productoList.set(i, productToUpdate);
                return;
            }
        }
    }

    public void removeProducto(int id) {
        for (ProductoModel producto: productoList) {
            if (producto.getId() == id) {
                productoList.remove(producto);
                return;
            }
        }
    }

    public UsuarioModel addUsuarioProducto(int idProducto, UsuarioModel usuarioModel) {
        ProductoModel producto = this.getProducto(idProducto);
        if (producto != null) {
            usuarioModel.setId(this.secuenciaIdsUsuarios.incrementAndGet());
            producto.getUsuarios().add(usuarioModel);
            return usuarioModel;
        }

        return null;
    }

    public UsuarioModel updateUsuarioProducto(int idProducto, int idUsuario, UsuarioModel usuarioToUpdate) {
        ProductoModel producto = this.getProducto(idProducto);
        if (producto != null) {
            for (int i = 0; i < producto.getUsuarios().size(); i++) {
                UsuarioModel usuario = producto.getUsuarios().get(i);
                if (usuario.getId() == idUsuario) {
                    //actualizamos el id del usuario que reemplazará al antiguo
                    usuarioToUpdate.setId(idUsuario);
                    producto.getUsuarios().set(i, usuarioToUpdate);
                    return usuarioToUpdate;
                }
            }
        }

        return null;
    }

    public ProductoModel deleteUsuarioProducto(int idProducto, int idUsuario) {
        ProductoModel producto = this.getProducto(idProducto);
        if (producto != null) {
            for (int i = 0; i < producto.getUsuarios().size(); i++) {
                UsuarioModel usuario = producto.getUsuarios().get(i);
                if (usuario.getId() == idUsuario) {
                    //eliminamos el usuario encontrado
                    producto.getUsuarios().remove(usuario);
                    return producto;
                }
            }
        }

        return null;
    }
}

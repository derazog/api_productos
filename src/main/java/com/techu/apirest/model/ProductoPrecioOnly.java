package com.techu.apirest.model;

public class ProductoPrecioOnly {

    private double precio;

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
}

package com.techu.apirest.model;

import java.util.ArrayList;
import java.util.List;

public class ProductoModel {

    private int id;
    private double precio;
    private String descripcion;
    private List<UsuarioModel> usuarios = new ArrayList<>();

    public ProductoModel() {

    }

    public ProductoModel(int id, double precio, String descripcion, List<UsuarioModel> usuarios) {
        this.id = id;
        this.precio = precio;
        this.descripcion = descripcion;
        this.usuarios = usuarios;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<UsuarioModel> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<UsuarioModel> usuarios) {
        this.usuarios = usuarios;
    }
}

package com.techu.apirest.controller;

import com.techu.apirest.model.ProductoModel;
import com.techu.apirest.model.UsuarioModel;
import com.techu.apirest.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("${api.version}/productos")
public class UsuarioController {

    @Autowired
    private ProductoService productService;

    @GetMapping("/{idProducto}/usuarios")
    public ResponseEntity getProductIdUsers(@PathVariable int idProducto) {
        ProductoModel producto = productService.getProducto(idProducto);
        if (producto == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(producto.getUsuarios());
    }

    @PostMapping("/{idProducto}/usuarios")
    public ResponseEntity addUsuarioProducto(@PathVariable int idProducto,
                                             @RequestBody UsuarioModel usuarioModel) {
        UsuarioModel usuario = productService.addUsuarioProducto(idProducto, usuarioModel);
        if (usuario == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        } else {
            return ResponseEntity.ok(usuario);
        }
    }

    @PutMapping("/{idProducto}/usuarios/{idUsuario}")
    public ResponseEntity updateProducto(@PathVariable int idProducto,
                                         @PathVariable int idUsuario,
                                         @RequestBody UsuarioModel usuarioToUpdate) {
        UsuarioModel usuario = productService.updateUsuarioProducto(idProducto, idUsuario, usuarioToUpdate);
        if (usuario == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        } else {
            return ResponseEntity.ok(usuario);
        }
    }

    @DeleteMapping("/{idProducto}/usuarios/{idUsuario}")
    public ResponseEntity deleteProducto(@PathVariable int idProducto, @PathVariable int idUsuario) {
        ProductoModel pr = productService.deleteUsuarioProducto(idProducto, idUsuario);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}

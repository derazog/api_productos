package com.techu.apirest.controller;

import com.techu.apirest.model.ProductoModel;
import com.techu.apirest.model.ProductoPrecioOnly;
import com.techu.apirest.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${api.version}/productos")
public class ProductoController {

    @Autowired
    private ProductoService productService;

    @GetMapping
    public ResponseEntity getProductos() {
        List<ProductoModel> productos = productService.getProductos();
        return ResponseEntity.ok(productos);
    }

    @GetMapping("/{id}")
    public ResponseEntity getProductoPorId(@PathVariable int id) {
        ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }

    @PostMapping
    public ResponseEntity addProducto(@RequestBody ProductoModel productoModel) {
        productService.addProducto(productoModel);
        //Producto creado satisfactoriamente
        return ResponseEntity.ok(productoModel);
    }

    @PutMapping("/{id}")
    public ResponseEntity updateProducto(@PathVariable int id,
                                         @RequestBody ProductoModel productToUpdate) {
        ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productService.updateProducto(id, productToUpdate);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteProducto(@PathVariable int id) {
        ProductoModel pr = productService.getProducto(id);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productService.removeProducto(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping("/{id}")
    public ResponseEntity patchPrecioProducto(
            @RequestBody ProductoPrecioOnly productoPrecioOnly, @PathVariable int id){
        ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        pr.setPrecio(productoPrecioOnly.getPrecio());
        productService.updateProducto(id, pr);
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }
}

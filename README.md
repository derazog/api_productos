# API Productos
API Rest con operaciones CRUD de productos como parte del Entregable 01 del Practicioner Backend

## Instalacion local
* Ejecutar el comando git: **git clone https://derazog@bitbucket.org/derazog/api_productos.git** para clonar el repositorio 
  e importar el proyecto mvn en un IDE.
* Configurar el comando mvn: **spring-boot:run** en el IDE.
* Iniciar la aplicación.

## Documentación
* Una vez iniciada la aplicación, ingresar a la url: **http://localhost:8090/swagger-ui.html** desde un navegador web.
* Para realizar pruebas desde Postman, importar el proyecto ubicado en la ruta:
  **/api_productos/project_files/Entregable 01 - Practicioner Backend.postman_collection.json**